$(document).ready(function(){
    //Popum Modal 
    $('.modal').modal();
    $('.dropdown-trigger').dropdown();
    
    // Dropmenu 
    $(function() {   
    $(".dropdown-trigger").dropdown({
        inDuration: 400,
        outDuration: 100,
        closeOnClick:  true,
        belowOrigin: true, // Displays dropdown below the button
        alignment: 'right' // Displays dropdown with edge aligned to the left of button
    });
    });

    // ToolTipped
    $(function(){
        $('.tooltipped').tooltip({
        });
    });

    // Select form    
    $(function() {   
      $('select').formSelect({
      hover: true,
    });
    
    });

    // Date and Time   
    $('.datepicker').datepicker({
      container: 'body'      
    });
    $('.datepicker02').datepicker({
      container: 'body'      
    });
    $('.timepicker').timepicker({
      container: 'body'      
    });
    $('.timepicker02').timepicker({
      container: 'body'      
    });

    $('.datepickerMonth').datepicker({
      format: 'mm-yyyy',
      container: 'body', 
      selectYears: 99,
      selectMonths: true,
    });
    

    // Collapsible        
    $('.collapsible').collapsible();

    $(function() {
      $('.showBroker').hide();
      $('#BrokerName').change(function(){
        $('#' + $(this).val()).show();
      });
    });   




});

     


// Sidebar Nav

$(document).ready(function(){
    $(function(){
        $('.sidenav').sidenav();
    });

    $('.fullScreen').click(function(){
      $('.sidenav').css('transform', 'translateX(-105%)')      
      $('.sidebar-nav').removeClass('sidenav-fixed');
      $('.sidenav').removeClass('sidenav-fixed');
      $('.fullScreen').css('display','none');
      $('.ShowfullScreen').css('display','block');
    });
    $('.ShowfullScreen').click(function(){
      $('.sidenav').css('transform', 'translateX(0%)')      
      $('.sidebar-nav').addClass('sidenav-fixed');
      $('.sidenav').addClass('sidenav-fixed');
      $('.ShowfullScreen').css('display','none');
      $('.fullScreen').css('display','block');
    });

});

// Tabs Jquery

$(document).ready(function(){
  $('.tabs').tabs();
});


// Side menu Fixed

$(document).ready(function(){

  $('.sideNavClick').click(function(){

    if ($("#slide-out").hasClass("sidenav-fixed")) {
        $('.content-wrapper').css('padding-left','207px')
    }else{
      $('.content-wrapper').css('padding-left','0')
    }  

  });

});


$(document).ready(function(){

if ($("#slide-out").hasClass("sidenav-fixed")) {
    $('.content-wrapper').css('padding-left','207px')
}else{
  $('.content-wrapper').css('padding-left','0')
}

});






// Listing Filter Jquery

$(document).ready(function(){

  $('#origin_destination').on('click', function(e){
    $('.dropFilt').css('display', 'block');
    $('.dropFiltDate').hide('slow');
    e.stopPropagation();       
  });

  $('.dropFilt').click(function(e){
    e.stopPropagation();
  })

  $('body').click(function(){
    $('.dropFilt').hide('slow');
  })

  $('#closeIcBtn').on('click', function(){
    $('.dropFilt').hide('slow');
  });


// date filter //

  $('#from_todate').on('click', function(e){
    $('.dropFiltDate').css('display', 'block');
    $('.dropFilt').hide('slow');
    e.stopPropagation();

    $('.datepicker-modal').click(function(e){
      e.stopPropagation();
    });
   

  });



  $('.dropFiltDate').click(function(e){
    e.stopPropagation();
  })

  $('body').click(function(){
    $('.dropFiltDate').hide('slow');
  })

  $('#closeIcBtn2').on('click', function(){
    $('.dropFiltDate').hide('slow');
  });  


// dateTime filter //  

  $('#from_dateTime01').on('click', function(e){
    $('.dropFiltDate01').css('display', 'block');
    $('.dropFiltDate02').hide('slow');
    e.stopPropagation();       
  });

  $('.dropFiltDate01').click(function(e){
    e.stopPropagation();
  })

  $('body').click(function(){
    $('.dropFiltDate01').hide('slow');
  })

$('#closeIcBtn01').on('click', function(){
  $('.dropFiltDate01').hide('slow');
});


// dateTime filter //  

  $('#to_dateTime02').on('click', function(e){
    $('.dropFiltDate02').css('display', 'block');
    $('.dropFiltDate01').hide('slow');
    e.stopPropagation();       
  });

  $('.dropFiltDate02').click(function(e){
    e.stopPropagation();
  })

  $('body').click(function(){
    $('.dropFiltDate02').hide('slow');
  })

$('#closeIcBtn02').on('click', function(){
  $('.dropFiltDate02').hide('slow');
});



});


function myoriginDest() {
  var x = document.getElementById("origin").value;
  var y = document.getElementById("destination").value;
  if (x=="" && y=="") {
    document.getElementById("origin_destination").innerHTML = "Start - Ent Point";
  }
  else if(x!="" && y!=""){
    document.getElementById("origin_destination").innerHTML = "  " + x  + " - " + "  " + y ;
  }
  else if(x!="" && y==""){
    document.getElementById("origin_destination").innerHTML = "  " + x + " (start point) ";
  }
  else if(x=="" && y!=""){
    document.getElementById("origin_destination").innerHTML = "  " + y + " (end point) ";
  }
}


// date filter //

function myfromtoDate() {
  var x = document.getElementById("fromDate").value;
  var y = document.getElementById("toDate").value;
  if (x=="" && y=="") {
    document.getElementById("from_todate").innerHTML = "From - To Date";
  }
  else if(x!="" && y!=""){
    document.getElementById("from_todate").innerHTML = "  " + x  + " - " + "  " + y ;
  }
  else if(x!="" && y==""){
    document.getElementById("from_todate").innerHTML = "  " + x + " (from date) ";
  }
  else if(x=="" && y!=""){
    document.getElementById("from_todate").innerHTML = "  " + y + " (to date) ";
  }
}


// dateTime filter //

function myfromtoDateTime01() {
  var x = document.getElementById("fromDate").value;
  var y = document.getElementById("formtime").value;
  if (x=="" && y=="") {
    document.getElementById("from_dateTime01").innerHTML = "From - Date & Time";
  }
  else if(x!="" && y!=""){
    document.getElementById("from_dateTime01").innerHTML = "  " + x  + " - " + "  " + y ;
  }
  else if(x!="" && y==""){
    document.getElementById("from_dateTime01").innerHTML = "  " + x + " (from date) ";
  }
  else if(x=="" && y!=""){
    document.getElementById("from_dateTime01").innerHTML = "  " + y + " (from time) ";
  }
}


function myfromtoDateTime02() {
  var x = document.getElementById("toDate").value;
  var y = document.getElementById("totime").value;
  if (x=="" && y=="") {
    document.getElementById("to_dateTime02").innerHTML = "To - Date & Time";
  }
  else if(x!="" && y!=""){
    document.getElementById("to_dateTime02").innerHTML = "  " + x  + " - " + "  " + y ;
  }
  else if(x!="" && y==""){
    document.getElementById("to_dateTime02").innerHTML = "  " + x + " (to date) ";
  }
  else if(x=="" && y!=""){
    document.getElementById("to_dateTime02").innerHTML = "  " + y + " (to time) ";
  }
}












// Mobile Listing Filter Jquery

$(document).ready(function(){

  $('#origin_destination002').on('click', function(e){
    $('.dropFilt').css('display', 'block');
    $('.dropFiltDate').hide('slow');
    e.stopPropagation();       
  });

  $('.dropFilt').click(function(e){
    e.stopPropagation();
  })

  $('body').click(function(){
    $('.dropFilt').hide('slow');
  })

  $('#closeIcBtn002').on('click', function(){
    $('.dropFilt').hide('slow');
  });


// date filter //

  $('#from_todate002').on('click', function(e){
    $('.dropFiltDate').css('display', 'block');
    $('.dropFilt').hide('slow');
    e.stopPropagation();

    $('.datepicker-modal').click(function(e){
      e.stopPropagation();
    });
   

  });



  $('.dropFiltDate').click(function(e){
    e.stopPropagation();
  })

  $('body').click(function(){
    $('.dropFiltDate').hide('slow');
  })

  $('#closeIcBtn003').on('click', function(){
    $('.dropFiltDate').hide('slow');
  });  


// dateTime filter //  

  $('#from_dateTime01').on('click', function(e){
    $('.dropFiltDate01').css('display', 'block');
    $('.dropFiltDate02').hide('slow');
    e.stopPropagation();       
  });

  $('.dropFiltDate01').click(function(e){
    e.stopPropagation();
  })

  $('body').click(function(){
    $('.dropFiltDate01').hide('slow');
  })

$('#closeIcBtn002').on('click', function(){
  $('.dropFiltDate01').hide('slow');
});





});


function myoriginDest002() {
  var x = document.getElementById("origin002").value;
  var y = document.getElementById("destination002").value;
  if (x=="" && y=="") {
    document.getElementById("origin_destination002").innerHTML = "Start - Ent Point";
  }
  else if(x!="" && y!=""){
    document.getElementById("origin_destination002").innerHTML = "  " + x  + " - " + "  " + y ;
  }
  else if(x!="" && y==""){
    document.getElementById("origin_destination002").innerHTML = "  " + x + " (start point) ";
  }
  else if(x=="" && y!=""){
    document.getElementById("origin_destination002").innerHTML = "  " + y + " (end point) ";
  }
}


// date filter //

function myfromtoDate002() {
  var x = document.getElementById("fromDate002").value;
  var y = document.getElementById("toDate002").value;
  if (x=="" && y=="") {
    document.getElementById("from_todate002").innerHTML = "From - To Date";
  }
  else if(x!="" && y!=""){
    document.getElementById("from_todate002").innerHTML = "  " + x  + " - " + "  " + y ;
  }
  else if(x!="" && y==""){
    document.getElementById("from_todate002").innerHTML = "  " + x + " (from date) ";
  }
  else if(x=="" && y!=""){
    document.getElementById("from_todate002").innerHTML = "  " + y + " (to date) ";
  }
}





$(document).ready(function() {
  var max_fields      = 100; //maximum input boxes allowed
  var wrapper         = $(".input_fields_wrap"); //Fields wrapper
  var add_button      = $(".add_field_button"); //Add button ID
  
  var x = 1; //initlal text box count
  $(add_button).click(function(e){ //on add input button click
      e.preventDefault();
      if(x < max_fields){ //max input box allowed
          x++; //text box increment
          $(wrapper).append('<div id="DivRe"><div class="input-field col"><select id="origin" onchange="myoriginDest()"><option value="" selected>List of Origin</option><option value="Pepsi Parwano">Pepsi Parwano</option><option value="Pepsi phagwal">Pepsi phagwal</option><option value="Pepsi Dehradun">Pepsi Dehradun</option><option value="Pepsi Sahibabad">Pepsi Sahibabad</option><option value="Pepsi Varanasi">Pepsi Varanasi</option><option value="Pepsi Narela">Pepsi Narela</option><option value="Pepsi Jaipur">Pepsi Jaipur</option></select></div><div class="input-field col"><select id="destination" onchange="myoriginDest()"><option value="" selected>List of Destination</option><option value="Pepsi Parwano">Pepsi Parwano</option><option value="Pepsi phagwal">Pepsi phagwal</option><option value="Pepsi Dehradun">Pepsi Dehradun</option><option value="Pepsi Sahibabad">Pepsi Sahibabad</option><option value="Pepsi Varanasi">Pepsi Varanasi</option><option value="Pepsi Narela">Pepsi Narela</option><option value="Pepsi Jaipur">Pepsi Jaipur</option></select></div></div><a href="javascript:void(0)" class="remove_field btn btn-danger btn-sm"><i class="fa fa-times-circle fa-fw"></i> Remove</a></div>');
      }
  });
  
  $(wrapper).on("click",".remove_field", function(e){ //user click on remove text
      e.preventDefault(); $(this).parent('#DivRe').remove(); x--;
  })
});







// Customer Checked

$(document).ready(function(){

    $('#agrCheck').click(function(){
        $('#TcPv').prop('checked', true);
    });

    $('#agrCheck2').click(function(){
        $('#TcPv').prop('checked', true);
    });

    $('#allCustCheck').click(function(){
      $('.listOfFilter input:checkbox').not(this).prop('checked', this.checked);
    });

$('#custlo').click(function(){
    if( $('#custlo').prop('checked')){
      $('#custShow').css('display','block');
    }else{
      $('#custShow').css('display','none');
    }
});


$('#tollCheck').click(function(){
  if( $('#tollCheck').prop('checked')){
    $('#toolPlaza').modal('open');
  }else{
  }  
})


});


// Alert Message

$(document).ready(function(){
  $('.tap-target').tapTarget('open');
});



// Window Scroll page hide Customer Section
/*
$(window).scroll(function() {
  if ($(".navTop").offset().top > 50) {
      $('.listOfFilter').slideUp();
  } else {
  }
});
*/



// Searching Filter

$(document).ready(function(){

    $('.CustSelBrn').on('click', function(e){
        $('.listOfFilter').slideToggle();
        e.stopPropagation();  

        $('.datepicker-modal').click(function(e){
          e.stopPropagation();
        });
                 
      });

      $('.listOfFilter').click(function(e){
        e.stopPropagation();
      })

    $('.closeSFlt').on('click', function(){
        $('.listOfFilter').slideToggle('close')
    });
    
    $('body').click(function(){
      $('.listOfFilter').hide('slow');
    })


});


// Table Collapse 

$(document).ready(function(){
  $('#rowOne').click(function(){
    $('#tableDiv').slideToggle();
  });
});


// $(function() {
//   $('.tableListing td span').click(function() {
//     $(this).val();
//     $(this).focus();
//     $(this).select();
//     document.execCommand('copy');
//     console.log(this);

//   });
// });





// select Customer pop

$(document).ready(function(){

$('.iNclickPop').on('click', function(){
  var a = $("#AindCust").val();
  if( a!=""){
    $('#favoriteIndent').modal('open');
  }else{
    $('#SelectIndent').modal('open');
  }
});


$('#Adhoc').click(function(){
  if( $('#Adhoc').prop('checked')){
    $('#adhocShow').css('display','block');
  }else{
    $('#adhocShow').css('display','none');
  }
});



$('#LoEnt').click(function(){
  if( $('#LoEnt').prop('checked')){
    $('#LoEnShow').css('display','block');
  }else{
    $('#LoEnShow').css('display','none');
  }
});

$('#UpEnt').click(function(){
  if( $('#UpEnt').prop('checked')){
    $('#upEnShow').css('display','block');
  }else{
    $('#upEnShow').css('display','none');
  }
});


$('.ckLRN').on('click', function(){
  var a = $("#lrNoSe").val();
  if( a!=""){
    $('#LRDetailsPanel').css('display', 'block');
  }else{
    $('#SelectIndent').modal('open');
  }
});


$('#IsMarket').click(function(){
  if($('#IsMarket').prop('checked')){
    $('#OpenIsMarket').css('display', 'block');
    $('#CheckIsMarket').css('display', 'none');
    $('#CheckIsMarket2').css('display', 'none');
  }else{
    $('#OpenIsMarket').css('display', 'none');
    $('#CheckIsMarket').css('display', 'block');
    $('#CheckIsMarket2').css('display', 'block');
  }
})


$('#PayViaEnt').click(function(){
  if($('#PayViaEnt').prop('checked')){
    $('#PayViewPanel').css('display', 'block');
  }else{
    $('#PayViewPanel').css('display', 'none');
  }
})


$('#SelTyreCond').change(function(){
  var a = $('#SelTyreCond').val();
  if( a=='new' ){
    $('#newTyre').css('display','block');
    $('#usedTyre').css('display','none');
  } else if( a=='used' ){
    $('#usedTyre').css('display','block');
    $('#newTyre').css('display','none');
  }
})

});




/* Please fill all required fields
$(document).ready(function(){
  var a = $('.modal input').val();

$('.modal [type="submit"]').on('click', function(){
  if(a == ''){
    $('#errorModal').modal('open');
  }else{
  }
});

});
*/



$(document).ready(function(){

  $('#editVal').on('click', function(){
    $('.editsm').css('display','block');
    $('.hdsm').css('display','none');
    $('#editVal').css('display','none');
  });

  $('#cancelVal').on('click', function(){
    $('.editsm').css('display','none');
    $('.hdsm').css('display','block');
    $('#editVal').css('display','block');
  });

})









//Upload File Javascript
$(document).ready(function(){
  // Basic
  $('.dropify').dropify();

  // Used events
  var drEvent = $('#input-file-events').dropify();
  drEvent.on('dropify.beforeClear', function(event, element){
      return confirm("Do you really want to delete \"" + element.file.name + "\" ?");
  });

  drEvent.on('dropify.afterClear', function(event, element){
      alert('File deleted');
  });
  drEvent.on('dropify.errors', function(event, element){
      console.log('Has Errors');
  });

  var drDestroy = $('#input-file-to-destroy').dropify();
  drDestroy = drDestroy.data('dropify')
  $('#toggleDropify').on('click', function(e){
      e.preventDefault();
      if (drDestroy.isDropified()) {
          drDestroy.destroy();
      } else {
          drDestroy.init();
      }
  })
});























// Map View Script

function initMap(){
  var option={
    zoom: 17,
    center: {lat:28.504790, lng: 77.086210}
  }

  var map = new
  google.maps.Map(document.getElementById('map'), option);

  var marker = new google.maps.Marker({
    position:{lat:28.504790, lng: 77.086210},
    map:map,
    icon:'http://www.gobolt.in/images/logo_color.png'
  });


  var InfoWindow = new google.maps.InfoWindow({
    content: '<div style="text-align:center"><h5 style="margin:0;">Go Bolt</h5><br/><h6 style="margin:0;">Camions Logistics Solution Pvt Ltd.</h6></div>'
  });

  marker.addListener('click', function(){
    InfoWindow.open(map, marker);
  });



}







    // scroll to top

$(document).ready(function () {

    $(window).scroll(function () {
        if ($(this).scrollTop() > 50) {
            $('.trackingView').addClass('mapFT');
        } else {
            $('.trackingView').removeClass('mapFT');
        }
    });

    //Click event to scroll to top
    $('.scrollToTop').click(function () {
        $('html, body').animate({scrollTop: 0}, 800);
        return false;
    });

});


// Scroll top menu active
$(window).scroll(function(){
  if ($(this).scrollTop() > 100) {
     $('.MBnav').addClass('stickyTop');
  } else {
     $('.MBnav').removeClass('stickyTop');
  }
});

$(function(){
  $('.myDateList li a').on( 'click', function() {
        $(this).addClass('active');
  });
});




/*

var table = '#mytable';
$('#maxRows').on('change', function () {
	$('.pagination').html('')
	var trnum = 0;
	var maxRows = parseInt($(this).val());
	var totalRows = $(table+' tbody tr').lenght;
	$(table+' tr:gt(0)').each(function () {
		trnum++
		if (trnum > maxRows) {
			$(this).hide()
		}if (trnum <= maxRows) {
			$(this).show()
		}
	});
	if (totalRows > maxRows) {
		var pagenum = Math.ceil(totalRows/maxRows)
		for (var i = 1; i <= pagenum;) {
			$('.pagination').append('<li data-page="'+i+'">\<span>'+ i++ +'<span class="sr-only">(current)</span></span>\</li>').show()
		}
	}
	$('.pagination li:first-child').addClass('active')
	$('pagination li').on('click', function () {
		var pagenum = $(this).attr('data-page')
		var trIndex = 0
    $('pagination li').removeClass('active')
    $(this).addClass('active')
		$(table+' tr:gt(0)').each(function () {
			trIndex++
			if (trIndex > (maxRows*pagenum) || trIndex <= ((maxRows)-maxRows)) {
				$(this).hide()
			}else{
				$(this).show()
			}
		})
	})
});


*/
















// Highcharts Script

Highcharts.chart('tableChart', {
    chart: {
      type: 'column'
    },
    title: {
      text: 'Monthly Average Tracking'
    },
    subtitle: {
      text: 'Source: Gobolt.in'
    },
    credits: {
        enabled: false
    },
    xAxis: {
      categories: [
        'Jan',
        'Feb',
        'Mar',
        'Apr',
        'May',
        'Jun',
        'Jul',
        'Aug',
        'Sep',
        'Oct',
        'Nov',
        'Dec'
      ],
      crosshair: true
    },
    yAxis: {
      min: 0,
      title: {
        text: 'Camions Logistics Solution'
      }
    },
    tooltip: {
      headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
      pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
        '<td style="padding:0"><b>{point.y:.1f} mm</b></td></tr>',
      footerFormat: '</table>',
      shared: true,
      useHTML: true
    },
    plotOptions: {
      column: {
        pointPadding: 0.2,
        borderWidth: 0
      }
    },
    series: [{
      name: 'Flipkart',
      data: [49.9, 71.5, 106.4, 129.2, 144.0, 176.0, 135.6, 148.5, 216.4, 194.1, 95.6, 54.4]
  
    }, {
      name: 'Amazon',
      data: [83.6, 78.8, 98.5, 93.4, 106.0, 84.5, 105.0, 104.3, 91.2, 83.5, 106.6, 92.3]
  
    }, {
      name: 'Pepperfry',
      data: [48.9, 38.8, 39.3, 41.4, 47.0, 48.3, 59.0, 59.6, 52.4, 65.2, 59.3, 51.2]
  
    }, {
      name: 'Philips',
      data: [42.4, 33.2, 34.5, 39.7, 52.6, 75.5, 57.4, 60.4, 47.6, 39.1, 46.8, 51.1]
  
    }],
    responsive: {
      rules: [{
          condition: {
              maxWidth: 500
          },
          // Make the labels less space demanding on mobile
          chartOptions: {
              xAxis: {
                  labels: {
                      formatter: function () {
                          return this.value.charAt(0);
                      }
                  }
              },
              yAxis: {
                  labels: {
                      align: 'left',
                      x: 0,
                      y: -2
                  },
                  title: {
                      text: ''
                  }
              }
          }
      }]
  }    
  });






// Highcharts Script

  Highcharts.chart('lineChart', {
    chart: {
      type: 'areaspline'
    },
    subtitle: {
      text: 'Source: Gobolt.in'
    },    
    title: {
      text: 'Average Tracking one week'
    },
    credits: {
        enabled: false
    },    
    legend: {
      layout: 'vertical',
      align: 'left',
      verticalAlign: 'top',
      x: 150,
      y: 100,
      floating: true,
      borderWidth: 1,
      backgroundColor: (Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'
    },
    xAxis: {
      categories: [
        'Monday',
        'Tuesday',
        'Wednesday',
        'Thursday',
        'Friday',
        'Saturday',
        'Sunday'
      ],
      plotBands: [{ // visualize the weekend
        from: 4.5,
        to: 6.5,
        color: 'rgba(68, 170, 213, .2)'
      }]
    },
    yAxis: {
      title: {
        text: 'Camions Logistics Solution'
      }
    },
    tooltip: {
      shared: true,
      valueSuffix: 'Truck'
    },
    credits: {
      enabled: false
    },
    plotOptions: {
      areaspline: {
        fillOpacity: 0.5
      }
    },
    series: [{
      name: 'Flipkart',
      data: [33, 4, 3, 5, 4, 10, 12]
    }, {
      name: 'Amazon',
      data: [1, 3, 4, 3, 3, 5, 4]
    }],
    responsive: {
      rules: [{
          condition: {
              maxWidth: 500
          },
          // Make the labels less space demanding on mobile
          chartOptions: {
              xAxis: {
                  labels: {
                      formatter: function () {
                          return this.value.charAt(0);
                      }
                  }
              },
              yAxis: {
                  labels: {
                      align: 'left',
                      x: 0,
                      y: -2
                  },
                  title: {
                      text: ''
                  }
              }
          }
      }]
  }    
  });



  

  Highcharts.chart('IndentChart', {

    chart: {
      type: 'heatmap',
      marginTop: 40,
      marginBottom: 80,
      plotBorderWidth: 1
    },
  
      credits: {
        enabled: false
    }, 
    title: {
      text: 'Indent per weekday'
    },
  
    xAxis: {
      categories: ['Total Indent', 'Runing Indent', 'Pending Indent', 'Field Indent', 'Success Indent']
    },
  
    yAxis: {
      categories: ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday'],
      title: null
    },
  
    colorAxis: {
      min: 0,
      minColor: '#FFFFFF',
      maxColor: Highcharts.getOptions().colors[0]
    },
  
    legend: {
      align: 'right',
      layout: 'vertical',
      margin: 0,
      verticalAlign: 'top',
      y: 25,
      symbolHeight: 280
    },
  
    tooltip: {
      formatter: function () {
        return '<b>' + this.series.xAxis.categories[this.point.x] + '</b> sold <br><b>' +
          this.point.value + '</b> items on <br><b>' + this.series.yAxis.categories[this.point.y] + '</b>';
      }
    },
  
    series: [{
      name: 'Sales per employee',
      borderWidth: 1,
      data: [[0, 0, 52], [0, 1, 19], [0, 2, 8], [0, 3, 24], [0, 4, 67], [1, 0, 25], [1, 1, 58], [1, 2, 78], [1, 3, 117], [1, 4, 48], [2, 0, 7], [2, 1, 15], [2, 2, 123], [2, 3, 64], [2, 4, 52], [3, 0, 3], [3, 1, 132], [3, 2, 114], [3, 3, 19], [3, 4, 16], [4, 0, 17], [4, 1, 5], [4, 2, 8]],
      dataLabels: {
        enabled: true,
        color: '#000000'
      }
    }],
    plotOptions: {
      series: {
          cursor: 'pointer',
          point: {
              events: {
                  click: function () {
                      $('#indentValueModal').modal('open');
                  }
              }
          }
      }
  },    
    responsive: {
      rules: [{
          condition: {
              maxWidth: 500
          },
          // Make the labels less space demanding on mobile
          chartOptions: {
              xAxis: {
                  labels: {
                      formatter: function () {
                          return this.value.charAt(0);
                      }
                  }
              },
              yAxis: {
                  labels: {
                      align: 'left',
                      x: 0,
                      y: -2
                  },
                  title: {
                      text: ''
                  }
              }
          }
      }]
  }    
  
    
  });






























