


/*===================================
    sidenav 
  ===================================*/


function openNav() {
  document.getElementById("mySidenav").style.width = "280px";
}
function closeNav() {
  document.getElementById("mySidenav").style.width = "0";
}

function openNav2() {
  document.getElementById("mySidenavRight").style.width = "280px";
}
function closeNav2() {
  document.getElementById("mySidenavRight").style.width = "0";
}

function openFilter() {
  document.getElementById("AdvanceFilter").style.width = "340px";
}
function closeFilter() {
  document.getElementById("AdvanceFilter").style.width = "0";
}



/*===================================
    navbar menu 
  ===================================*/



/*===================================
    Tooltip 
  ===================================*/

$(function () {
  $('[data-toggle="tooltip"]').tooltip()
})



/*===================================
    Table Data View 
  ===================================*/




/*===================================
    Fitler Remove 
  ===================================*/



$("#selectBox option[value='option1']").remove();

$(document).ready(function(){
  $('#selectLR').select({
    allowClear: true,
    minimumResultsForSearch: -1,
    width: 600
  });
});





$('#date,#date2,#date3,#date4,#date5,#date6,#date7,#date8,#date9,#date10').bootstrapMaterialDatePicker({
  time: false,
  clearButton: true
});

$('#time,#time2,#time3,#time4,#time5,#time6,#time7,#time8,#time9,#time10').bootstrapMaterialDatePicker({
  date: false,
  shortTime: false,
  format: 'HH:mm'
});



/*===================================
    File Upload  
  ===================================*/

$(document).ready(function(){

'use strict';

;( function( $, window, document, undefined ){
  $( '.inputfile' ).each( function()
  {
    var $input   = $( this ),
      $label   = $input.next( 'label' ),
      labelVal = $label.html();

    $input.on( 'change', function( e )
    {
      var fileName = '';

      if( this.files && this.files.length > 1 )
        fileName = ( this.getAttribute( 'data-multiple-caption' ) || '' ).replace( '{count}', this.files.length );
      else if( e.target.value )
        fileName = e.target.value.split( '\\' ).pop();

      if( fileName )
        $label.find( 'span' ).html( fileName );
      else
        $label.html( labelVal );
    });

    // Firefox bug fix
    $input
    .on( 'focus', function(){ $input.addClass( 'has-focus' ); })
    .on( 'blur', function(){ $input.removeClass( 'has-focus' ); });
  });
})( jQuery, window, document );

});


